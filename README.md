Destroy
===========

Symptoms
-------------
User would like to remove stubborn file(s) and/or folder(s) that are problem causing on their computer.

Cause
-------------
Sometimes files and/or folders cause problems with how the OS functions. Removing items may be difficult, especially if they are in use and the user might not have full permissions to remove the items (such as not having "wheel" group permissions).

Installation/Usage Instructions
-------------
Here comes Destroy, a simple Mac script that allows you do remove the problem causing files and/or folders easy.

**Before continuing**: This program uses the command `rm -rf` and may run as **SUDO**. Anytime you run a command as **SUDO**, you are running it with elevated permissions and anything can occurr with the computer, including data loss. Also, since this program uses the command `rm -rf`, there is always a chance that the files will be unrecoverable. Please make sure you know how the `rm` command works before running this script.

Destroy uses the rm command (UNIX) to remove files and unlink them to files that are already in use. The flag `-rf` tells the computer that:
* `-r`: which removes directories, removing the contents recursively beforehand (so as not to leave files without a directory to reside in) ("recursive")*
* `-f`: which ignores non-existent files and overrides any confirmation prompts ("force"), although it will not remove files from a directory if the directory is write protected.

**Steps:**


1.  Download the file [here](https://bitbucket.org/hsmac/destroy-mac-app/downloads/Destroy.zip).
2.  Close any opened programs (or just make sure that everything is saved).
3.  Open **Destroy**.
4.  Upon opening the app, you will be asked: ![alt text](https://bitbucket.org/hsmac/mac-scripts/raw/18c4bd373b47fdad985d93c0f06d57f5de4af999/images/destroy/capture1.png "Capture1")
5.  Select **File** or **Folder**.
6.  You will then be prompted to select the item(s) and then select **Choose**.
7.  If the program was able to use user permissions, then the file will disappear and you will receive a message saying **Deleted**. If the program was not able to use user permissions, then it will display the error that was given and ask if you would like to run as **SUDO**. 
8.  Type in your **SUDO** password.
9.  Then the file will disappear and you will receive a message saying **Deleted**.

Related
-------------
* http://en.wikipedia.org/wiki/Rm_(Unix)