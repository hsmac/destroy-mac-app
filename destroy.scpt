FasdUAS 1.101.10   ��   ��    k             l     ��  ��      Destroy for Mac     � 	 	     D e s t r o y   f o r   M a c   
  
 l     ��  ��    1 + Author: Kyle Keilson (kkeilson@depaul.edu)     �   V   A u t h o r :   K y l e   K e i l s o n   ( k k e i l s o n @ d e p a u l . e d u )      l     ��  ��    $  Date Modified: 15 August 2013     �   <   D a t e   M o d i f i e d :   1 5   A u g u s t   2 0 1 3      l     ��������  ��  ��        l     ��  ��     Define Variables     �     D e f i n e   V a r i a b l e s      j     �� �� 0 	pass_word    m        �         !   l     ��������  ��  ��   !  " # " l     $���� $ I    �� % &
�� .sysodlogaskr        TEXT % m      ' ' � ( ( > W h a t   w o u l d   y o u   l i k e   t o   d e s t r o y ? & �� ) *
�� 
btns ) J     + +  , - , m     . . � / /  F i l e -  0�� 0 m     1 1 � 2 2  F o l d e r��   * �� 3 4
�� 
disp 3 m    ����   4 �� 5��
�� 
dflt 5 m   	 
���� ��  ��  ��   #  6 7 6 l   6 8���� 8 Z    6 9 :�� ; 9 =    < = < l    >���� > n     ? @ ? 1    ��
�� 
bhit @ l    A���� A 1    ��
�� 
rslt��  ��  ��  ��   = m     B B � C C  F i l e : r    $ D E D I    ���� F
�� .sysostdfalis    ��� null��   F �� G H
�� 
prmp G m     I I � J J 2 S e l e c t   f i l e s   t o   d e s t r o y :   H �� K��
�� 
mlsl K m    ��
�� boovtrue��   E o      ���� 0 x  ��   ; r   ' 6 L M L I  ' 2���� N
�� .sysostflalis    ��� null��   N �� O P
�� 
prmp O m   ) , Q Q � R R 6 S e l e c t   f o l d e r s   t o   d e s t r o y :   P �� S��
�� 
mlsl S m   - .��
�� boovtrue��   M o      ���� 0 x  ��  ��   7  T U T l     ��������  ��  ��   U  V W V l  7 C X���� X Z  7 C Y Z���� Y =  7 : [ \ [ 1   7 8��
�� 
rslt \ m   8 9��
�� boovfals Z L   = ?����  ��  ��  ��  ��   W  ]�� ] l  D � ^���� ^ X   D � _�� ` _ Q   Z � a b c a I  ] n�� d��
�� .sysoexecTEXT���     TEXT d b   ] j e f e m   ] ` g g � h h  r m   - r f   f n   ` i i j i 1   e i��
�� 
strq j n   ` e k l k 1   a e��
�� 
psxp l o   ` a���� 0 thefile theFile��   b R      �� m��
�� .ascrerr ****      � **** m o      ���� 0 errfile errFile��   c k   v � n n  o p o I  v ��� q r
�� .sysodlogaskr        TEXT q b   v � s t s m   v y u u � v v � I t e m   c a n n o t   b e   d e l e t e d   w i t h   u s e r   p e r m i s s i o n s .   E s c a l a t i n g   s e s s i o n   w i t h   S U D O & 
 	 	 
 	 	 P a t h :   t n   y � w x w 1   ~ ���
�� 
strq x n   y ~ y z y 1   z ~��
�� 
psxp z o   y z���� 0 thefile theFile r �� { |
�� 
btns { J   � � } }  ~�� ~ m   � �   � � �  O K��   | �� � �
�� 
disp � m   � �����  � �� ���
�� 
dflt � m   � ����� ��   p  ��� � I  � ��� � �
�� .sysoexecTEXT���     TEXT � b   � � � � � m   � � � � � � �  s u d o   r m   - r f   � n   � � � � � 1   � ���
�� 
strq � n   � � � � � 1   � ���
�� 
psxp � o   � ����� 0 thefile theFile � �� � �
�� 
RApw � o   � ����� 0 	pass_word   � �� ���
�� 
badm � m   � ���
�� boovtrue��  ��  �� 0 thefile theFile ` o   G J���� 0 x  ��  ��  ��       �� �  ���   � ������ 0 	pass_word  
�� .aevtoappnull  �   � **** � �� ����� � ���
�� .aevtoappnull  �   � **** � k     � � �  " � �  6 � �  V � �  ]����  ��  ��   � ������ 0 thefile theFile�� 0 errfile errFile � ! '�� . 1������������ B�� I�������� Q�������� g���������� u  �����
�� 
btns
�� 
disp
�� 
dflt�� 
�� .sysodlogaskr        TEXT
�� 
rslt
�� 
bhit
�� 
prmp
�� 
mlsl�� 
�� .sysostdfalis    ��� null�� 0 x  
�� .sysostflalis    ��� null
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
psxp
�� 
strq
�� .sysoexecTEXT���     TEXT�� 0 errfile errFile��  
�� 
RApw
�� 
badm�� �����lv�j�k� O��,�  *���e� E` Y *�a �e� E` O�f  hY hO o_ [a a l kh   a �a ,a ,%j W @X  a �a ,a ,%�a kv�l�k� Oa �a ,a ,%a b   a  e� [OY��ascr  ��ޭ